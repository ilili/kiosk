package app.kiosk.repositories;

import app.kiosk.persistence.Tariff;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by jt on 1/10/17.
 */
public interface ProductRepository extends CrudRepository<Tariff, Long> {}
