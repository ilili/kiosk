package app.kiosk.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity @Data @EqualsAndHashCode(callSuper=false)
@Table(name = "Tariffs")
public class Tariff extends BaseObject {

	@Column(name = "ORIGINAL_ID") @Setter
	private Long originalId;

	@Column(name = "NAME", unique = true, length = 64, nullable = false)
	private String name;

	@Column(name = "PRICE", nullable = false, columnDefinition = "DOUBLE(10,2) DEFAULT 0.00")
	private Double price;

	@Column(name = "AVAILABLE_OPTIONS")
	private String availableOptions;

	@Column(name = "DESCRIPTION")
	private String description;
}
