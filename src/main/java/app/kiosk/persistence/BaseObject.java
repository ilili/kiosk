package app.kiosk.persistence;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@MappedSuperclass
public abstract class BaseObject {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Basic @Column(name = "ACTIVE_UNTIL")
	protected Timestamp activeUntil;

	@Basic @Column(name = "RECIEVED")
	protected Timestamp recieved;
}
