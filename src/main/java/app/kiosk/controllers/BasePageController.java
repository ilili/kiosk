package app.kiosk.controllers;

import app.kiosk.services.TariffService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by jt on 1/10/17.
 */
@Controller
public class BasePageController {

	private static final Logger log = LogManager.getLogger(BasePageController.class);

	private TariffService tariffService;
	@Autowired ObjectMapper objectMapper;

	@Autowired
	public void setTariffService(TariffService tariffService) {
		this.tariffService = tariffService;
	}

	@GetMapping(value = "/")
	public String redirectToList() {
		return "redirect:/jsf/index.xhtml";
	}

	@GetMapping("/error")
	public String showError() {
		return "redirect:/jsf/error.xhtml";
	}

	public void updatePage(String jsonMessage) {
		log.info("publish: " + jsonMessage);
	}
}