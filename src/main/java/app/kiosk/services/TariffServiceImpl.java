package app.kiosk.services;

import app.kiosk.config.RabbitConfig;
import app.kiosk.dao.TariffDao;
import app.kiosk.dao.TariffListDao;
import app.kiosk.persistence.Tariff;
import app.kiosk.repositories.TariffRepository;
import app.kiosk.utils.FieldsHelper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by jt on 1/10/17.
 */
@Service
public class TariffServiceImpl implements TariffService {

	private static final Logger log = LoggerFactory.getLogger(TariffServiceImpl.class);

	@Autowired RabbitConfig rabbitConfig;

	private TariffRepository tariffRepository;
//	private RabbitTemplate rabbitTemplate;

	@Autowired
	public TariffServiceImpl(TariffRepository tariffRepository
//	                         RabbitTemplate rabbitTemplate
	                        ) {
		this.tariffRepository = tariffRepository;
//		this.rabbitTemplate = rabbitTemplate;
	}


	@Override
	public List<Tariff> listAll() {
		List<Tariff> tariffs = new ArrayList<>();
		tariffRepository.findAll().forEach(tariffs::add); //fun with Java 8
		return tariffs;
	}

	@Override
	public Tariff getById(Long id) {
		return tariffRepository.findById(id).orElse(null);
	}

	@Override
	public Tariff saveOrUpdate(Tariff tariff) {
		tariffRepository.save(tariff);
		return tariff;
	}

	private Tariff getPersistenceTariff(@NotNull TariffDao tariffDao) {
		Tariff tariff = FieldsHelper.copyProperties(tariffDao, new Tariff());
		tariff.setOriginalId(tariffDao.getId());
		tariff.setRecieved(Timestamp.valueOf(LocalDateTime.now()));
		return tariff;
	}


	private List<Tariff> getPersistenceTariffListByAction(TariffListDao tld, String... actionFlag) {
		List<Tariff> tariffList = new ArrayList<>();
		if (tld != null) {
			tld.getTariffs()
			   .forEach(tariffDao -> {
				   if (StringUtils.contains(Arrays.toString(actionFlag), tariffDao.getAction())) {
					   tariffList.add(getPersistenceTariff(tariffDao));
				   }
			   });
		}
		return tariffList;
	}

	public void doSomething(TariffListDao tld) {
		try {
			if ("full".equals(tld.getPackageType())) {
				tariffRepository.deleteAll();
			} else {
				List<Tariff> newTariffsToDelete = getPersistenceTariffListByAction(tld, "remove");
				List<Long> idsToDelete = newTariffsToDelete.stream()
				                                           .map(Tariff::getOriginalId)
				                                           .filter(Objects::nonNull)
				                                           .collect(Collectors.toList());
				List<Tariff> persistentTariffsToDelete = tariffRepository.getAllByOriginalIdIn(idsToDelete);
				tariffRepository.deleteAll(persistentTariffsToDelete);
			}
		}
		catch (Exception ex) {
			log.error("Can't delete table values: " + ex.getMessage());
		}

		List<Tariff> tariffListToAdd = getPersistenceTariffListByAction(tld, "add", "update");
		try {
			tariffRepository.saveAll(tariffListToAdd);
		}
		catch (Exception ex) {
			log.error("Can't add or update tariff values: " + ex.getMessage());
		}
	}

	@Override
	public void delete(Long id) {
		tariffRepository.deleteById(id);
	}
//
//	@Override public Tariff saveOrUpdateProductForm(ProductForm productForm) {
//		return null;
//	}

//	@Override
//	public Tariff saveOrUpdateProductForm(ProductForm productForm) {
//		Tariff savedTariff = saveOrUpdate(productFormToProduct.convert(productForm));
//
//		System.out.println("Saved Product Id: " + savedTariff.getId());
//		return savedTariff;
//	}
//
//	@Override
//	public void sendProductMessage(String id) {
//		Map<String, String> actionmap = new HashMap<>();
//		actionmap.put("id", id);
//		log.info("Sending the index request through queue message");
//
//		Message jsonMessage = MessageBuilder.withBody(id.getBytes())
//		                                    .andProperties(MessagePropertiesBuilder.newInstance().setContentType("application/json")
//		                                                                           .build()).build();
//		rabbitTemplate.send(rabbitConfig.getIncomingQueueName(), jsonMessage);
//	}

	//
//	public void sendProductMessageText(String messageText) {
////		Map<String, String> actionmap = new HashMap<>();
////		actionmap.put("id", message);
//		log.info("Sending the index request through queue message");
//		MessageProperties props = new MessageProperties();
//		props.getHeaders().put("ddd", 100);
////	setAppId("Self-service");
////		props.setCorrelationId(UUID.randomUUID().toString());
//		props.setContentType(CONTENT_TYPE_JSON);
////		props.setContentEncoding(DEFAULT_CHARSET.displayName());
//
//		Message message = new Message(messageText.getBytes(), props);
//		rabbitTemplate.convertAndSend(rabbitConfig.getExchangeName(), rabbitConfig.getIncomingQueueName(), message);
////		rabbitTemplate.send(rabbitConfig.getIncomingQueueName(), message);
////		rabbitTemplate.send(message);
//	}
//@Autowired/
//ObjectMapper
//	public void sendProductMessageD(TariffListDao json) {
////		Map<String, TariffListDao> actionmap = new HashMap<>();
////		actionmap.put("id", t);
////		log.info("Sending the index request through queue message");
//		MessageProperties props = new MessageProperties();
//		props.getHeaders().put("ddd", 100);
//		props.setAppId("Self-service");
//		props.setCorrelationId(UUID.randomUUID().toString());
//		props.setContentType(CONTENT_TYPE_JSON);
//		props.setContentEncoding(DEFAULT_CHARSET.displayName());
////		String json = "{\"foo\" : \"value\" }";
////		objectMapper
//		Message jsonMessage = MessageBuilder.withBody(json.toString().getBytes())
//		                                    .andProperties(MessagePropertiesBuilder.newInstance().setContentType("application/json")
//		                                                                           .build()).build();
////		Message message = new Message(json.getBytes(), props);
////		rabbitTemplate.convertAndSend(rabbitConfig.getExchangeName(), rabbitConfig.getIncomingQueueName(), t);
////		rabbitTemplate.send(rabbitConfig.getExchangeName(), rabbitConfig.getIncomingQueueName(), json);
//		rabbitTemplate.convertAndSend(rabbitConfig.getExchangeName(), rabbitConfig.getIncomingQueueName(), jsonMessage);
//		System.out.println(jsonMessage);
////		rabbitTemplate.convertAndSend(t);
////		rabbitTemplate.send(rabbitConfig.getIncomingQueueName(), message);
////		rabbitTemplate.send(message);
//	}
}
