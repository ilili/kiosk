package app.kiosk.services;

import app.kiosk.persistence.Tariff;

import java.util.List;

/**
 * Created by jt on 1/10/17.
 */
public interface TariffService {

	List<Tariff> listAll();

	Tariff getById(Long id);

	Tariff saveOrUpdate(Tariff tariff);

	void delete(Long id);

//	Tariff saveOrUpdateProductForm(ProductForm productForm);

//	void sendProductMessage(String id);
//	void sendProductMessageD(TariffListDao id);
}
