package app.kiosk.utils;

import java.nio.charset.StandardCharsets;

public class MessageFormat {

	public static String bytesToString(byte[] bytes) {
		if (bytes != null && bytes.length > 0) {
			return new String(bytes, 0, bytes.length, StandardCharsets.UTF_8);
		}
		return "";
	}
}
