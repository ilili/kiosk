package app.kiosk;

import app.kiosk.config.RabbitConfig;
import app.kiosk.listeners.TariffListener;
import com.fasterxml.jackson.annotation.JacksonInject;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

// (exclude = {ErrorMvcAutoConfiguration.class})
@SpringBootApplication
public class MainApp {
	@JacksonInject
	private final RabbitConfig rabbitConfig;

	public MainApp(RabbitConfig rabbitConfig) {this.rabbitConfig = rabbitConfig;}

	public static void main(String[] args) {
		SpringApplication.run(MainApp.class, args);
	}

	@Bean
	Queue queue() {
		return new Queue(rabbitConfig.getIncomingQueueName(), rabbitConfig.isIncomingQueueDurable());
	}

	@Bean
	TopicExchange exchange() {
		return new TopicExchange(rabbitConfig.getExchangeName(),
		                         rabbitConfig.isExchangeDurable(),
		                         rabbitConfig.isAutodeleteable());
	}

	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(rabbitConfig.getIncomingQueueName());
	}

	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
	                                         MessageListenerAdapter listenerAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(rabbitConfig.getIncomingQueueName());
		container.setMessageListener(listenerAdapter);
		return container;
	}

	@Bean
	MessageListenerAdapter listenerAdapter(TariffListener receiver) {
		return new MessageListenerAdapter(receiver, "receiveMessage");
	}

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/jsf/");
		viewResolver.setSuffix(".xhtml");
		return viewResolver;
	}
}