package app.kiosk.listeners;

import app.kiosk.MainApp;
import app.kiosk.controllers.BasePageController;
import app.kiosk.dao.TariffListDao;
import app.kiosk.repositories.TariffRepository;
import app.kiosk.services.TariffServiceImpl;
import app.kiosk.utils.MessageFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Queue listener class, its receiveMessage() method invoked with the message as the parameter.
 */
@Component
public class TariffListener extends Jackson2JsonMessageConverter {

	private static final Logger log = LogManager.getLogger(TariffListener.class);

	private TariffRepository tariffRepository;
	private ObjectMapper objectMapper;
	private TariffServiceImpl tariffService;

	@Autowired
	public void setTariffService(TariffServiceImpl tariffService) {
		this.tariffService = tariffService;
	}

	private BasePageController basePageController;

	@Autowired
	public void setBasePageController(BasePageController basePageController) {
		this.basePageController = basePageController;
	}

	@Autowired
	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	public TariffListener(TariffRepository tariffRepository) {
		this.tariffRepository = tariffRepository;
	}

	/**
	 * This method is invoked whenever any new message is put in the queue. See {@link MainApp} for more details
	 *
	 * @param bytes bytes array
	 */
	public void receiveMessage(byte[] bytes) {
		String textMessage = MessageFormat.bytesToString(bytes);

		try {
			TariffListDao tld = objectMapper.readValue(bytes, TariffListDao.class);
			log.info(() -> "Received:\n" + textMessage);

			tariffService.doSomething(tld);
			basePageController.updatePage(textMessage);
		}
		catch (IOException e) {
			log.error(() -> "Wrong format message recieved:\n" + textMessage);
		}
	}
}
