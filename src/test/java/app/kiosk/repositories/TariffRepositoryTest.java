package app.kiosk.repositories;

import app.kiosk.persistence.Tariff;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TariffRepositoryTest {

	private static final BigDecimal BIG_DECIMAL_100 = BigDecimal.valueOf(100.00);
	private static final String PRODUCT_DESCRIPTION = "a cool product";
	private static final String IMAGE_URL = "http://an-imageurl.com/image1.jpg";

	@Autowired
	private ProductRepository productRepository;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testPersistence() {
		//given
		Tariff tariff = new Tariff();

		//when
		productRepository.save(tariff);

		//then
		Assert.assertNotNull(tariff.getId());
		Tariff newTariff = productRepository.findById(tariff.getId()).orElse(null);
		Assert.assertEquals((Long) 1L, newTariff.getId());
	}
}